package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }

    @Override
    public boolean connected(V u, V v) {
      Stack<V> stack = new Stack<>();
    Set<V> visited = new HashSet<>();
    stack.push(u);
    while (!stack.isEmpty()) {
        V node = stack.pop();
        if (!visited.contains(node)) {
            visited.add(node);
            if (node.equals(v)) {
                return true;
            }
            for (V neighbor : graph.getNeighbourhood(node)) {
                if (!visited.contains(neighbor)) {
                    stack.push(neighbor);
                }
            }
        }
    }
    return false;
    }

}
